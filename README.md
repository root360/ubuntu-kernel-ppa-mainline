## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/root360-ubuntu-kernel-ppa-mainline.asc https://root360.gitlab.io/ubuntu-kernel-ppa-mainline/gpg.key
```

## Add repo to apt

```bash
apt-get install apt-transport-https
echo "deb [arch=amd64] https://root360.gitlab.io/ubuntu-kernel-ppa-mainline mainline main" | sudo tee /etc/apt/sources.list.d/root360-ubuntu-kernel-ppa-mainline.list
```

## Installation

```bash
# latest
sudo apt-get install linux-mainline
# specific version
sudo apt-get install linux-mainline-5.9
```
